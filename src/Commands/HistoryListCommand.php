<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Service\HistoryServiceInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $this->signature = sprintf(
            '%s {id=all : The id to be %s} {--d|driver=composite : %s}',
            $this->getCommandVerb(),
            $this->getCommandPassiveVerb(),
            $this->getCommandOptionVerb(),
        );

        $this->description = sprintf('List the history of given calculations');

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history:list';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'listed';
    }

    protected function getCommandOptionVerb(): string
    {
        return 'Select the driver (file, latest, composite) to store calculations';
    }

    protected function getArgument(): string
    {
        return $this->argument('id');
    }

    protected function getOption(): string
    {
        return $this->option('driver');
    }

    public function handle(HistoryServiceInterface $service): void
    {
        $result = $service->list($this->getArgument(), $this->getOption());
        $headers = ['ID', 'Command', 'Operation', 'Result'];
        $this->table($headers, $result);
    }
}
