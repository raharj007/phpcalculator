<?php


namespace Jakmall\Recruitment\Calculator\Commands;


class SubtractCommand extends CommandHandler
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'subtract';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'subtracted';
    }
}
