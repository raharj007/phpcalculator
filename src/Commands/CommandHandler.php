<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Service\CalculateServiceInterface;

class CommandHandler extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($commandVerb));

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'command_verb';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'command_passive_verb';
    }

    public function handle(CalculateServiceInterface $service): void
    {
        $command = $this->getCommandVerb();
        $numbers = $this->getInput();
        $calculate = $service->run($command, $numbers);
        $this->comment(sprintf('%s = %s', $calculate->operation, $calculate->result));
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }
}
