<?php


namespace Jakmall\Recruitment\Calculator\Commands;


use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Service\HistoryServiceInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    public function __construct()
    {
        $this->signature = sprintf(
            '%s {id=all : The id to be %s}',
            $this->getCommandVerb(),
            $this->getCommandPassiveVerb(),
        );

        $this->description = sprintf('Clear the history of given calculations');

        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'history:clear';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'cleared';
    }

    protected function getArgument(): string
    {
        return $this->argument('id');
    }

    public function handle(HistoryServiceInterface $service): void
    {
        $arg = $this->getArgument();

        if ($arg == 'all') {
            $success_message = 'All history is cleared';
            $error_message = 'Failed to remove all history';
        } else {
            $success_message = sprintf('Data with ID %s is removed', $arg);
            $error_message = sprintf('Failed to remove history with id: %s', $arg);
        }

        $result = $service->destroy($arg);
        if ($result) $this->comment($success_message);
        else $this->comment($error_message);
    }
}
