<?php

namespace Jakmall\Recruitment\Calculator\Commands;


class AddCommand extends CommandHandler
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'add';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'added';
    }
}
