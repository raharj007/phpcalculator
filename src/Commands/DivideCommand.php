<?php


namespace Jakmall\Recruitment\Calculator\Commands;


class DivideCommand extends CommandHandler
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'divide';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'divided';
    }
}
