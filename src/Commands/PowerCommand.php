<?php


namespace Jakmall\Recruitment\Calculator\Commands;


class PowerCommand extends CommandHandler
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'power';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'powered';
    }
}
