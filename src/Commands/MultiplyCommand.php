<?php


namespace Jakmall\Recruitment\Calculator\Commands;


class MultiplyCommand extends CommandHandler
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'multiply';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'multiplied';
    }
}
