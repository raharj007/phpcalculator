<?php


namespace Jakmall\Recruitment\Calculator\History\Infrastructure;



class CommandHistoryManager implements CommandHistoryManagerInterface
{
    protected $filesystem;
    protected $state;
    public function __construct()
    {
        $this->filesystem = __DIR__.'/../../../storage/logs/';
        $this->state = __DIR__.'/../../../storage/state/autoincrement.json';
    }

    protected function autoIncrement(): int
    {
        // open latest id from the state
        $content = json_decode(file_get_contents($this->state));
        // make sure that the id is integer
        $latest = (int)$content->latest_id;
        // increment the id
        $latest += 1;
        // save the new id into the state
        file_put_contents($this->state, json_encode(["latest_id" => $latest]));
        // return the id
        return $latest;
    }

    protected function resetID(): void
    {
        file_put_contents($this->state, json_encode(["latest_id" => 0]));
    }

    protected function setFile($driver = ''): string
    {
        $string = '';
        if ($driver != '') {
            switch ($driver) {
                case 'file':
                    $string = $this->getFileDriver();
                    break;
                case 'latest':
                    $string = $this->getLatestDriver();
                    break;
                default:
                    break;
            }
        }
        return $string;
    }

    protected function getFileDriver(): string{
        return'mesinhitung.log';
    }

    protected function getLatestDriver(): string{
        return 'latest.log';
    }

    protected function fileToArray($path): array
    {
        $path = $this->filesystem.$path;
        $content = file_get_contents($path);
        return explode("\n", $content);
    }

    protected function findInStorage($driver, $method = 0, $id = null, $str = '')
    {
        $result = []; $temp = [];
        $content = $this->fileToArray($driver);
        foreach ($content as $item) {
            if ($item != null) {
                $array = explode(',', $item);
                // $method 0 is for get all data
                if ($method == 0) $result[] = $array;
                elseif ($method == 1) {
                    // $method 1 is for get data by id or operation string
                    if (($id != null && $array[0] == $id) || ($str != '' && $str == $array[2])) {
                        // if the data and id are the same
                        // save the item to temporary variable for next reoder
                        // into the top of the list if the driver is latest
                        $content = $item;
                        // this variable is for the result
                        $result[] = $array;
                        // if the driver is not latest then return the result
                        if ($driver == $this->getFileDriver()) break;
                    } else {
                        // if the data and id didn't match, save the data for
                        // next reorder if the driver is latest
                        $temp[] = $item;
                    }
                } elseif ($method == 2) {
                    // $method 2 is for destroy data by id, this method only save
                    // the item that not match with id
                    if ($array[0] != $id) $result[] = $item;
                }
            }
        }

        // if $method => 1 or $method => 2, then format data into object
        // because the return value is more than 1
        if ($method == 1) {
            $result = (object)[
                'content' => $content,
                'result' => $result,
                'temp' => $temp,
            ];
        } elseif ($method == 2) {
            $result = (object)[
                'result' => $result,
                'rcount' => count($result),
                'ccount' => count($content),
            ];
        }

        return $result;
    }

    /**
     * Returns array of command history.
     *
     * @param string $driver
     *
     * @return array returns an array of commands in storage
     */
    public function findAll($driver): array
    {
        // TODO: Implement findAll() method.
        $file = $this->setFile($driver);
        // in case of finding all data, if the driver is not specified, then
        // the default driver is a file driver
        $driver = $file == '' ? $this->getFileDriver() : $file;
        return $this->findInStorage($driver);
    }

    /**
     * Find a command by id.
     *
     * @param string $driver
     * @param string|int $id
     *
     * @return null|mixed returns null when id not found.
     */
    public function find($driver, $id)
    {
        // TODO: Implement find() method.
        $file = $this->setFile($driver);
        if ($file == '') {
            // if the driver is default/composite, find data from the latest driver first
            $data = $this->findInStorage($this->getLatestDriver(), 1, $id);
            if ($data->result == []) {
                // if data is not found, then find data from the file driver
                $data = $this->findInStorage($this->getFileDriver(), 1, $id);
            } else {
                // order data to top if the driver is latest and result is not empty
                $new_content = implode("\n", $data->temp);
                $new_content = $data->content.PHP_EOL . $new_content.PHP_EOL;
                file_put_contents($this->filesystem.$this->getLatestDriver(), $new_content);
            }
        } else {
            // if the driver is not default/composite, find data from that chosen driver
            $data = $this->findInStorage($file, 1, $id);
        }

        return $data->result;
    }

    /**
     * Log command data to storage.
     *
     * @param mixed $command The command to log.
     *
     * @return bool Returns true when command is logged successfully, false otherwise.
     */
    public function log($command): bool
    {
        // TODO: Implement log() method.
        try {
            $id = $this->autoIncrement();

            $path_to_file_file = $this->filesystem.$this->getFileDriver();
            $content_file = file_get_contents($path_to_file_file);
            $content_file .= sprintf('%s,%s,%s,%s'.PHP_EOL, $id, $command->cmd, $command->opr, $command->res);
            file_put_contents($path_to_file_file, $content_file);

            $path_to_file_latest = $this->filesystem.$this->getLatestDriver();

            $data_latest = $this->findInStorage($this->getLatestDriver(), 1, null, $command->opr);
            $t_content_latest = $this->fileToArray($this->getLatestDriver());

            $count_of_content = count($t_content_latest);
            // if count of the latest list is 10 or more, then delete the oldest data
            // if new data is exist in storage, move data to the top of the list
            // and change the id with new id
            if ($count_of_content >= 10 && $data_latest->result == []) {
                array_splice($t_content_latest, 9, 1);
            } elseif ($count_of_content >= 10 && $data_latest != []
                        || $count_of_content < 10 && $data_latest->result != []) {
                $t_content_latest = $data_latest->temp;
            }

            // convert back array of the latest list into string
            $t_content_latest = implode("\n", $t_content_latest);

            $content_latest = sprintf('%s,%s,%s,%s'.PHP_EOL, $id, $command->cmd, $command->opr, $command->res);
            $content_latest .= $t_content_latest.PHP_EOL;
            file_put_contents($path_to_file_latest, $content_latest);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Clear a command by id
     *
     * @param string|int $id
     *
     * @return bool Returns true when data with $id is cleared successfully, false otherwise.
     */
    public function clear($id): bool
    {
        // TODO: Implement clear() method.
        $result = true;
        try {
            $file = $this->getFileDriver();
            $latest = $this->getLatestDriver();
            $file_data = $this->findInStorage($file, 2, $id);
            $latest_data = $this->findInStorage($latest, 2, $id);

            // store data only if id exist
            if ($file_data->rcount != ($file_data->ccount - 1)) {
                $new_content = implode("\n", $file_data->result).PHP_EOL;
                file_put_contents($this->filesystem.$file, $new_content);
                if ($file_data->result == []) $this->resetID();
            }

            if ($latest_data->rcount != ($latest_data->ccount - 1)) {
                $new_content = implode("\n", $latest_data->result).PHP_EOL;
                file_put_contents($this->filesystem.$latest, $new_content);
                if ($latest_data->result == []) $this->resetID();
            }

            if ($file_data->rcount == ($file_data->ccount - 1)
                && $latest_data->rcount == ($latest_data->ccount - 1)) $result = false;

        } catch (\Exception $exception) {
            return false;
        }
        return $result;
    }

    /**
     * Clear all data from storage.
     *
     * @return bool Returns true if all data is cleared successfully, false otherwise.
     */
    public function clearAll(): bool
    {
        // TODO: Implement clearAll() method.
        try {
            $file = $this->filesystem.$this->getFileDriver();
            $latest = $this->filesystem.$this->getLatestDriver();
            file_put_contents($file, '');
            file_put_contents($latest, '');
            $this->resetID();
        } catch (\Exception $exception) {
            return false;
        }
        return true;
    }
}
