<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Service\HistoryServiceInterface;
use Jakmall\Recruitment\Calculator\Service\ResponseServiceInterface;

class HistoryController
{
    /**
     * @var HistoryServiceInterface
     */
    protected $history;

    /**
     * @var ResponseServiceInterface
     */
    protected $response;

    public function __construct(HistoryServiceInterface $history, ResponseServiceInterface $response)
    {
        $this->history = $history;
        $this->response = $response;
    }

    public function index(Request $request)
    {
        // todo: modify codes to get history
        $driver = $request->driver ?? '';
        $result = $this->history->list('all', $driver, 'api');
        return $this->response->success($result);
    }

    public function show(Request $request, $id)
    {
        $driver = $request->driver ?? '';
        $result = $this->history->list($id, $driver, 'api');
        return $this->response->success($result);
    }

    public function remove($id)
    {
        // todo: modify codes to remove history
        $result = $this->history->destroy($id);
        $str_failed = sprintf('Failed to remove history with id: %s', $id);
        return $result ? $this->response->noContent() : $this->response->failed(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, $str_failed);
    }
}
