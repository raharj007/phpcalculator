<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Service\CalculateServiceInterface;
use Jakmall\Recruitment\Calculator\Service\ResponseServiceInterface;

class CalculatorController
{
    /**
     * @var CalculateServiceInterface
     */
    protected $service;

    /**
     * @var ResponseServiceInterface
     */
    protected $response;

    public function __construct(CalculateServiceInterface $service, ResponseServiceInterface $response)
    {
        $this->service = $service;
        $this->response = $response;
    }

    public function calculate(Request $request, $action)
    {
        try {
            $result = $this->service->run($action, $request->input);
            $response = $this->response->calculate($action, $result);
        } catch (\Exception $exception) {
            $response = $this->response->failed(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, $exception->getMessage());
        }

        return $response;
    }
}
