<?php


namespace Jakmall\Recruitment\Calculator\Service;


use Illuminate\Http\JsonResponse;

interface ResponseServiceInterface
{
    /**
     * @param $cmd
     * @param $res
     * @return JsonResponse
     */
    public function calculate($cmd, $res);

    /**
     * @param $data
     * @return JsonResponse
     */
    public function success($data);

    /**
     * @return JsonResponse
     */
    public function noContent();

    /**
     * @param $type
     * @param $msg
     * @return JsonResponse
     */
    public function failed($type, $msg);
}
