<?php


namespace Jakmall\Recruitment\Calculator\Service;


use Illuminate\Http\JsonResponse;

class ResponseService implements ResponseServiceInterface
{
    protected function calculateFormat($cmd, $res) {
        return (object)[
            'command' => $cmd,
            'operation' => $res->operation,
            'result' => $res->result,
        ];
    }

    /**
     * @param $cmd
     * @param $res
     * @return JsonResponse
     */
    public function calculate($cmd, $res) {
        $response = $this->calculateFormat($cmd, $res);
        return JsonResponse::create($response, JsonResponse::HTTP_OK);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public function success($data)
    {
        // TODO: Implement success() method.
        return JsonResponse::create($data, JsonResponse::HTTP_OK);
    }

    /**
     * @return JsonResponse
     */
    public function noContent()
    {
        // TODO: Implement noContent() method.
        return JsonResponse::create('', JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @param $status
     * @param $msg
     * @return JsonResponse
     */
    public function failed($status, $msg)
    {
        return JsonResponse::create($msg, $status);
    }
}
