<?php


namespace Jakmall\Recruitment\Calculator\Service;


interface HistoryServiceInterface
{
    /**
     * @param $arg
     * @param $opt
     * @param string $format
     * @return mixed
     */
    public function list($arg, $opt, $format = 'cli');

    /**
     * @param $arg
     * @return bool
     */
    public function destroy($arg): bool;
}
