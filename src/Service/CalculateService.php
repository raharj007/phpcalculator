<?php


namespace Jakmall\Recruitment\Calculator\Service;


use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CalculateService implements CalculateServiceInterface
{
    /**
     * @var object
     */
    protected $operator;

    /**
     * @var CommandHistoryManagerInterface
     */
    protected $history;

    public function __construct(CommandHistoryManagerInterface $history) {
        $this->operator = $this->setOperator();
        $this->history = $history;
    }

    protected function setOperator() {
        return (object)[
            'add' => '+',
            'divide' => '/',
            'multiply' => '*',
            'power' => '^',
            'subtract' => '-',
        ];
    }

    protected function objLogFormat($command, $operation, $result) {
        return (object)[
            'cmd' => $command,
            'opr' => $operation,
            'res' => $result,
        ];
    }

    /**
     * @param string $command
     * @param array $numbers
     *
     * @return object
     */
    public function run($command, array $numbers) {
        $this->operator->command = $this->operator->$command;
        $description = $this->generateCalculationDescription($numbers);
        $result = $this->calculateAll($numbers);
        $log_format = $this->objLogFormat($command, $description, $result);
        $this->history->log($log_format);
        return (object)[
            'operation' => $description,
            'result' => $result,
        ];
    }

    /**
     * @param string $command
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll($numbers) {
        $number = array_pop($numbers);
        if (count($numbers) <= 0) return $number;
        return $this->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @param string $command
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2) {
        switch ($this->operator->command) {
            case $this->operator->add : return $number1 + $number2;
            case $this->operator->divide : return $number1 / $number2;
            case $this->operator->multiply : return $number1 * $number2;
            case $this->operator->power : return $number1 ** $number2;
            case $this->operator->subtract : return $number1 - $number2;
        }
        return 0;
    }

    protected function generateCalculationDescription(array $numbers) {
        $glue = sprintf(' %s ', $this->operator->command);
        return implode($glue, $numbers);
    }
}
