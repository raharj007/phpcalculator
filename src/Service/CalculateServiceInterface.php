<?php


namespace Jakmall\Recruitment\Calculator\Service;


interface CalculateServiceInterface
{
    /**
     * @param string $command
     * @param array $numbers
     *
     * @return object
     */
    public function run($command, array $numbers);
}
