<?php


namespace Jakmall\Recruitment\Calculator\Service;


use Illuminate\Contracts\Container\Container;
use Jakmall\Recruitment\Calculator\Container\ContainerServiceProviderInterface;

class ServiceProvider implements ContainerServiceProviderInterface
{
    public function register(Container $container): void
    {
        $container->bind(ResponseServiceInterface::class, ResponseService::class);
        $container->bind(CalculateServiceInterface::class, CalculateService::class);
        $container->bind(HistoryServiceInterface::class, HistoryService::class);
    }
}
