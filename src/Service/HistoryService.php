<?php


namespace Jakmall\Recruitment\Calculator\Service;


use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryService implements HistoryServiceInterface
{
    /**
     * @var CommandHistoryManagerInterface
     */
    protected $history;

    public function __construct(CommandHistoryManagerInterface $history)
    {
        $this->history = $history;
    }

    protected function getInput($array): array {
        $temp = [];
        $input = explode(' ', $array[2]);
        foreach ($input as $key => $val) if ($key % 2 == 0) $temp[] = (int)$val;
        return $temp;
    }

    protected function apiResult($array): object {
        return (object)[
            'id' => $array[0],
            'command' => $array[1],
            'operation' => $array[2],
            'input' => $this->getInput($array),
            'result' => $array[3],
        ];
    }

    protected function apiResults($array): array {
        $result = [];
        foreach ($array as $item) $result[] = $this->apiResult($item);
        return $result;
    }

    /**
     * @param $arg
     * @param $opt
     * @return mixed
     */
    public function list($arg, $opt, $format = 'cli')
    {
        // TODO: Implement historyCLI() method.
        $result = [];
        try {
            $result = $arg == 'all' ? $this->history->findAll($opt)
                : $this->history->find($opt, (int)$arg);
            if ($format != 'cli') {
//                $result = $arg == 'all' ? $this->apiResults($result)
//                    : $this->apiResult($result[0]);
                $result = $arg == 'all' ? $this->apiResults($result)
                    : ($result[0] == null ? (object)[] : $this->apiResult($result[0]));
            }
        } catch (\Exception $exception) {
            return $result;
        }
        return $result;
    }

    /**
     * @param $arg
     * @return bool
     */
    public function destroy($arg): bool
    {
        // TODO: Implement destroy() method.
        try {
            $result = $arg == 'all' ? $this->history->clearAll()
                : $this->history->clear((int)$arg);
        } catch(\Exception $exception) {
            return false;
        }
        return $result;
    }
}
