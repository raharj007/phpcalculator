<?php

return [
    'providers' => [
        \Jakmall\Recruitment\Calculator\History\CommandHistoryServiceProvider::class,
        \Jakmall\Recruitment\Calculator\Service\ServiceProvider::class,
    ],
];
